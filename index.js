//khai báo thư viện express
const express = require('express');

//khai báo thư viện path để nối file
const path = require('path');

//khai báo cổng chạy
const port = 8000;

//khai báo app express
const app = express();

//khai báo middleware static để hiển thị hình ảnh
app.use(express.static(__dirname + "/views"));

app.get('/', (req, res) => {

    console.log("--" + __dirname + "--");

    res.sendFile(path.join(__dirname + ("/views/landingPage.html")));
})

//chạy app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})